# read the hcl file and the conf file
with open("main-job.hcl", "r") as f:
    hcl_file = f.readlines()

with open("telegraf.conf", "r") as f:
    conf_file = f.readlines()

location = -1
for index, line in enumerate(hcl_file):
    if "PLACE_CONFIG_HERE" in line:
        location = index

out_file = []
for ind in range(len(hcl_file)):
    if ind == location:
        out_file.extend(conf_file)
        out_file.extend("\n")
    else:
        out_file.append(hcl_file[ind])

with open("main-job.hcl", "w") as f:
    f.writelines(out_file)
