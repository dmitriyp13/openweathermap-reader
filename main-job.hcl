job "open-weathermap" {
    type = "service"

    group "main" {
        count = 1

        task "main-task" {
            driver = "docker"

            config {
                image = "telegraf:1.33.0-alpine"
                privileged = true
                mount {
                    type = "bind"
                    source = "local"
                    target = "/etc/telegraf"
                }
            }

            template {
                destination = "local/telegraf.conf"
                data = <<EOH
                    PLACE_CONFIG_HERE
                    EOH
            }

            template {
                destination = "${NOMAD_SECRETS_DIR}/file.env"
                env = true
                error_on_missing_key = true
                data = <<EOH
                    {{ with nomadVar "nomad/jobs/open-weathermap" }}
                    INFLUX_TOKEN="{{ .INFLUX_TOKEN | toJSON }}"
                    OPEN_WEATHERMAP_API_KEY = "{{ .OPEN_WEATHERMAP_API_KEY | toJSON }}"
                    {{ end }}
                    EOH
            }
        }
    } 
}